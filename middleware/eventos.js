module.exports = (app) => { 
   
    var EventosMiddleware = {

        buildMapping : function buildMapping(eventos) {
        
            eventos.createMapping((err, mapping) => {
                err ? console.log(err) : console.log("Mapping created! status: "+ mapping);
            });
            
            var stream  = eventos.synchronize();
            var count   = 0;

            stream.on('data', () => {
                count++;
            });

            stream.on('close', () => {
                console.log("Indexed " + count + " documents");
            });

            stream.on('error', (err) => {
                console.log(err);
            });
        }

    }
    return EventosMiddleware;
}
