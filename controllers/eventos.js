module.exports = (app) => {

	var Evento 		 	 = app.models.eventos;
	var middlewareEvento = app.middleware.eventos;

	middlewareEvento.buildMapping(Evento);

	var EventosController = {

        index: (req,res) => {
			Evento.find( (err,data) => {
				err ? console.log(err) : res.render("eventos/index", {lista: data, elastic : false});
			});
		},

		create: (req,res) => {
			res.render("eventos/create");
		},

		insert: (req,res) => {
			model = new Evento();
			model.titulo     = req.body.titulo;
			model.subtitulo  = req.body.subtitulo;
			model.dataEvento = req.body.dataEvento;
			model.descricao  = req.body.descricao;
			model.save( (err) => {
                err ? console.log(err) : res.redirect('/eventos',{elastic : false});
			});
		},

		edit: (req,res) => {
			Evento.findById(req.params.id, (err, data) => {
				err ? console.log(err) : res.render('eventos/edit', {value: data});
			});
		},

		update: (req,res) => {
			Evento.findById(req.params.id, (err, data) => {
				if(err){
					console.log(err);
				} else{
					var model   = data;
					model 	    = req.body;
					model.save( (err) => {
					    err ? console.log(err) : res.redirect('/eventos',{elastic : false});
					});
				}
			});
		},

		show: (req,res) => {
			Evento.findById(req.params.id, (err, data) => {
				err ? console.log(err) : res.render('eventos/show', {value: data});
			});
		},

		search : (req, res, next) => {
			if(req.query.q){
				Evento.search({
					query_string: {query: req.query.q},
				}, (err, results) => {
					if(err) return next(err);
					var data = results.hits.hits.map((hit) =>{
						return hit;
					});
					console.log(data);
					res.render('eventos/index', {
						query : req.query.q,
						lista  : data,
						elastic  : true
					})
				})
			}
			else{ res.render('eventos/index', {elastic : false }); }
		},

		add : (req, res) => {
			res.redirect("/search?q="+ req.body.q);
		}
    }

    return EventosController;
}