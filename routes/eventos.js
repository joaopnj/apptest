module.exports = (app) => {

    var eventos = app.controllers.eventos;

    // methods de HTTP, GET, POST, PUT , DELETE
	app.get ("/eventos",          eventos.index);
	app.get ("/search", 		  eventos.search);
	app.post("/search", 		  eventos.add);
	app.get ("/eventos/create",   eventos.create);
	app.post("/eventos",          eventos.insert);
	app.get ("/eventos/edit/:id", eventos.edit);
	app.post("/eventos/edit/:id", eventos.update);
	app.get ("/eventos/show/:id", eventos.show);
    
}